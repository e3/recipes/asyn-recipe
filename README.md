# asyn conda recipe

Home: https://github.com/epics-modules/asyn

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS module for driver and device support
